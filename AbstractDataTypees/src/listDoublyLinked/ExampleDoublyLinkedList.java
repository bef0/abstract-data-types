package listDoublyLinked;

public class ExampleDoublyLinkedList {

	public static void main(String[] args) {
		List dll = new List();
		System.out.println(dll.isEmpty()); // true
		System.out.println(dll.hasAccess()); // false
		dll.insert(null);
		System.out.println(dll.isEmpty()); // true
		dll.insert("Hello");
		System.out.println(dll.isEmpty()); // false
		System.out.println(dll.hasAccess()); // false
		dll.insert("World");
		dll.toFirst();
		System.out.println(dll.hasAccess()); // true
		System.out.println(dll.getObject()); // Hello
		dll.next();
		System.out.println(dll.getObject()); // null
		dll.toLast();
		dll.insert("World");
		System.out.println(dll.getObject()); // Hello
		dll.toFirst();
		System.out.println(dll.getObject()); // World
	}

}
