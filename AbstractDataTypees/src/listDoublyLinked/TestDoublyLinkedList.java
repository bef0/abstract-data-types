package listDoublyLinked;

import static org.junit.Assert.*;

import org.junit.*;

public class TestDoublyLinkedList {
		
	@Test 
	public void testToFirst() {
		List sll = new List();
		sll.append("a");
		sll.append("b");
		sll.append("c");
		sll.toFirst();
		assertTrue(sll.getObject().equals("a"));
	}

	@Test 
	public void testToLast() {
		List sll = new List();
		sll.append("a");
		sll.append("b");
		sll.append("c");
		sll.toLast();
		assertTrue(sll.getObject().equals("c"));
	}

	@Test
	public void testNext() {
		List sll = new List();
		sll.append("a");
		sll.append("b");
		sll.append("c");
		sll.toFirst();
		Object a = sll.getObject();
		sll.next();
		Object b = sll.getObject();
		assertTrue(a.equals("a") && b.equals("b"));
	}

	@Test 
	public void testSetObject() {
		List sll = new List();
		Object a = sll.getObject();
		sll.append("");
		sll.toFirst();
		sll.setObject("b");
		Object c = sll.getObject();
		assertTrue(a == null && c.equals("b"));
	}

	@Test 
	public void testGetObject() {
		List sll = new List();
		Object a = sll.getObject();
		sll.append("");
		Object b = sll.getObject();
		sll.toFirst();
		Object c = sll.getObject();
		assertTrue(a == null && b == null && c.equals(""));
	}

	@Test 
	public void testIsEmpty() {
		List sll = new List();
		boolean a = sll.isEmpty();
		sll.append("");
		boolean b = sll.isEmpty();
		assertTrue(a && !b);
	}

	@Test
	public void testHasAccess() {
		List sll = new List();
		boolean a = sll.hasAccess();
		sll.append("");
		boolean b = sll.hasAccess();
		sll.toFirst();
		boolean c = sll.hasAccess();
		sll.toLast();
		boolean d = sll.hasAccess();
		assertTrue(!a && !b && c && d);
	}

	@Test 
	public void testRemove() {
		List sll = new List();
		sll.remove();
		assertTrue(sll.isEmpty());
		sll.append("a");
		sll.remove();
		assertTrue(!sll.isEmpty());
		sll.append("b");
		sll.toFirst();
		sll.remove();
		sll.toFirst();
		assertTrue(sll.getObject().equals("b"));
		sll.next();
		sll.remove();
		assertTrue(sll.getObject().equals("b"));
		
		sll.insert("d");
		sll.toFirst();
		assertTrue(sll.getObject().equals("d"));
		sll.next();
		assertTrue(sll.getObject() != null);
		assertTrue(sll.getObject().equals("b"));
		
		sll.insert("e");
		sll.toFirst();
		sll.next();
//		sll.next();
		
		assertTrue(sll.getObject().equals("e"));
		sll.next();
		assertTrue(sll.getObject().equals("b"));
		sll.remove();
		sll.next();
		assertTrue(sll.getObject() == null);
	}

	@Test 
	public void testInsert() {
		List sll = new List();
		sll.insert(null);
		assertTrue(sll.isEmpty());
		sll.insert("a");
		sll.append("b");
		sll.toFirst();
		sll.insert("c");
		sll.toFirst();
		assertTrue(sll.getObject().equals("c"));
		sll.next();
		sll.insert("d");
		sll.toFirst();
		sll.next();
		assertTrue(sll.getObject().equals("d"));
		sll.next();
		sll.next();
		sll.insert("e");
		sll.toFirst();
		sll.next();
		sll.next();
		sll.next();
		assertTrue(sll.getObject().equals("e"));
	}

	@Test 
	public void testAppend() {
		List sll = new List();
		sll.append(null);
		assertTrue(sll.isEmpty());
		sll.append("a");
		sll.append("b");
		sll.toFirst();
		sll.append("c");
		sll.toLast();
		assertTrue(sll.getObject().equals("c"));
	}
	
	@Test
	public void testBefore() {
		List sll = new List();
		sll.append("a");
		sll.append("b");
		sll.append("c");
		sll.toLast();
		Object c = sll.getObject();
		sll.before();
		Object b = sll.getObject();
		assertTrue(c.equals("c") && b.equals("b"));
	}

	@Test
	public void testConcat() {
		List dll1 = new List();
		List dll2 = new List();
		
		dll1.concat(dll2);
		assertTrue(dll1.isEmpty());
		
		dll2.append("1");
		dll1.concat(dll2);
		dll1.toFirst();
		assertTrue(!dll1.isEmpty() && dll1.getObject().equals("1"));
		
		dll1 = new List();
		dll2 = new List();
		
		dll1.append("a");
		dll1.append("b");
		dll1.append("c");
		
		
		dll2.append("d");
		dll2.append("e");
		dll2.append("f");
		
		dll1.concat(dll2);
		dll1.toFirst();
		assertTrue(dll1.getObject().equals("a"));
		dll1.next();
		assertTrue(dll1.getObject().equals("b"));
		dll1.next();
		assertTrue(dll1.getObject().equals("c"));
		dll1.next();
		assertTrue(dll1.getObject().equals("d"));
		dll1.next();
		assertTrue(dll1.getObject().equals("e"));
		dll1.next();
		assertTrue(dll1.getObject().equals("f"));
		dll1.next();
		assertTrue(dll1.getObject() == null);
	}
}
