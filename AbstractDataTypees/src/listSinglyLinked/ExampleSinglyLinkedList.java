package listSinglyLinked;

public class ExampleSinglyLinkedList {

	public static void main(String[] args) {
		List sll = new List(); 
		System.out.println(sll.isEmpty()); // true
		System.out.println(sll.hasAccess()); // false
		sll.insert(null);
		System.out.println(sll.isEmpty()); // true
		sll.insert("Hello");
		System.out.println(sll.isEmpty()); // false
		System.out.println(sll.hasAccess()); // false
		sll.insert("World");
		sll.toFirst();
		System.out.println(sll.hasAccess()); // true
		System.out.println(sll.getObject()); // Hello
		sll.next();
		System.out.println(sll.getObject()); // null
		sll.toLast();
		sll.insert("World");
		System.out.println(sll.getObject()); // Hello
		sll.toFirst();
		System.out.println(sll.getObject()); // World
	}

}
