package listSinglyLinked;

/**
 * Singly linked list
 * 
 * @author bfuer
 *
 */
public class List {
	/**
	 * The first element in the list.
	 */
	private Node head;
	/**
	 * The last element in the list.
	 */
	private Node tail;
	/**
	 * The current elemnt in the list, also the element on which the list has the
	 * current access.
	 */
	private Node current;

	public void toFirst() {
		current = head;
	}

	public void toLast() {
		current = tail;
	}

	public void next() {
		if (hasAccess())
			current = current.getNext();
	}

	/**
	 * Set the object in the current element, if the list has access.
	 * 
	 * @param o is the new object for the element
	 */
	public void setObject(Object o) {
		if (o != null && hasAccess())
			current.setContent(o);
	}

	/**
	 * Get the object in the current element, if the list has access.
	 * 
	 * @return the object of the current element or <code>null</code>, if it hasn't
	 *         access
	 */
	public Object getObject() {
		if (!hasAccess())
			return null;
		return current.getContent();
	}

	public boolean isEmpty() {
		return head == null;
	}

	public boolean hasAccess() {
		return current != null;
	}

	/**
	 * Remove the current element in the list.
	 */
	public void remove() {
		// if, it hasn't access, stop the method
		if (!hasAccess())
			return;

		// current is the first element in the list
		if (current == head) {
			head = head.getNext();
			current = head;
		} else {
			Node before = getElementBefore(current);
			if (before == null) {
				throw new RuntimeException("The element could not be found.");
			}

			before.setNext(current.getNext());
			// if, it is the last element in the list:
			if (!current.hasNext()) {
				tail = before;
			}
		}
	}

	/**
	 * Insert the object before the current object.
	 * 
	 * @param o the object, that should be insert
	 */
	public void insert(Object o) {
		// if the object is null, you can't use it
		if (o == null) {
			return;
		}
		// if the list is empty, use append to add the object, so the function hasn't to
		// be
		// coded twice
		else if (isEmpty()) {
			append(o);
			return; // is finished
		}
		// if, it hasn't access, stop the method
		else if (!hasAccess()) {
			return;
		}
		// else: method does its work

		Node toInsert = new Node(o); // the new Node
		toInsert.setNext(current); // yes, we know that

		// current points on the begin
		if (current == head) {
			head = toInsert;
		}
		// current is somewhere
		else {
			Node before = getElementBefore(current);
			if (before == null) {
				throw new RuntimeException("The element could not be found.");
			}
			before.setNext(toInsert);
		}
	}

	/**
	 * Append the object at the end of the list.
	 * 
	 * @param o the object, what should be appended at the list.
	 */
	public void append(Object o) {
		// if the object is null, you can't use it
		if (o == null)
			return;

		Node toInsert = new Node(o); // the new Node
		// if the list is empty, the head has to set too
		if (isEmpty()) {
			head = toInsert;
		} else {
			tail.setNext(toInsert);
		}

		tail = toInsert;
	}

	private Node getElementBefore(Node n) {
		Node current = head;
		while (current.hasNext()) {
			Node next = current.getNext();
			if (next == n)
				return current;

			current = next;
		}

		return null;
	}

	public void concat(List e) {
		if (e == null || e.isEmpty())
			return;

		if (this.isEmpty()) {
			this.head = e.head;
			this.tail = e.tail;
		} else {
			tail.setNext(e.head);
			tail = e.tail;
		}
	}
}
