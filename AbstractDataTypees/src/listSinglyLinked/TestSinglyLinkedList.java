package listSinglyLinked;

import static org.junit.Assert.*;

import org.junit.*;

import listDoublyLinked.List;

public class TestSinglyLinkedList {
		
	@Test 
	public void testToFirst() {
		List sll = new List();
		sll.append("a");
		sll.append("b");
		sll.append("c");
		sll.toFirst();
		assertTrue(sll.getObject().equals("a"));
	}

	@Test 
	public void testToLast() {
		List sll = new List();
		sll.append("a");
		sll.append("b");
		sll.append("c");
		sll.toLast();
		assertTrue(sll.getObject().equals("c"));
	}

	@Test
	public void testNext() {
		List sll = new List();
		sll.append("a");
		sll.append("b");
		sll.append("c");
		sll.toFirst();
		Object a = sll.getObject();
		sll.next();
		Object b = sll.getObject();
		assertTrue(a.equals("a") && b.equals("b"));
	}

	@Test 
	public void testSetObject() {
		List sll = new List();
		Object a = sll.getObject();
		sll.append("");
		sll.toFirst();
		sll.setObject("b");
		Object c = sll.getObject();
		assertTrue(a == null && c.equals("b"));
	}

	@Test 
	public void testGetObject() {
		List sll = new List();
		Object a = sll.getObject();
		sll.append("");
		Object b = sll.getObject();
		sll.toFirst();
		Object c = sll.getObject();
		assertTrue(a == null && b == null && c.equals(""));
	}

	@Test 
	public void testIsEmpty() {
		List sll = new List();
		boolean a = sll.isEmpty();
		sll.append("");
		boolean b = sll.isEmpty();
		assertTrue(a && !b);
	}

	@Test
	public void testHasAccess() {
		List sll = new List();
		boolean a = sll.hasAccess();
		sll.append("");
		boolean b = sll.hasAccess();
		sll.toFirst();
		boolean c = sll.hasAccess();
		sll.toLast();
		boolean d = sll.hasAccess();
		assertTrue(!a && !b && c && d);
	}

	@Test 
	public void testRemove() {
		List sll = new List();
		sll.append("a");
		sll.append("b");
		sll.toFirst();
		sll.remove();
		sll.toFirst();
		assertTrue(sll.getObject().equals("b"));
		
		sll.insert("d");
		sll.toFirst();
		assertTrue(sll.getObject().equals("d"));
		sll.next();
		assertTrue(sll.getObject() != null);
		assertTrue(sll.getObject().equals("b"));
		
		sll.insert("e");
		sll.toFirst();
		sll.next();
//		sll.next();
		
		assertTrue(sll.getObject().equals("e"));
		sll.next();
		assertTrue(sll.getObject().equals("b"));
		sll.next();
		assertTrue(sll.getObject() == null);
	}

	@Test 
	public void testInsert() {
		List sll = new List();
		sll.append("a");
		sll.append("b");
		sll.toFirst();
		sll.insert("c");
		sll.toFirst();
		assertTrue(sll.getObject().equals("c"));
		sll.next();
		sll.insert("d");
		sll.toFirst();
		sll.next();
		assertTrue(sll.getObject().equals("d"));
		sll.next();
		sll.next();
		sll.insert("e");
		sll.toFirst();
		sll.next();
		sll.next();
		sll.next();
		assertTrue(sll.getObject().equals("e"));
	}

	@Test 
	public void testAppend() {
		List sll = new List();
		sll.append("a");
		sll.append("b");
		sll.toFirst();
		sll.append("c");
		sll.toLast();
		assertTrue(sll.getObject().equals("c"));
	}
	
	@Test
	public void testConcat() {
		List sll1 = new List();
		List sll2 = new List();
		
		sll1.concat(sll2);
		assertTrue(sll1.isEmpty());
		
		sll2.append("1");
		sll1.concat(sll2);
		sll1.toFirst();
		assertTrue(!sll1.isEmpty() && sll1.getObject().equals("1"));
		
		sll1 = new List();
		sll2 = new List();
		
		sll1.append("a");
		sll1.append("b");
		sll1.append("c");
		
		
		sll2.append("d");
		sll2.append("e");
		sll2.append("f");
		
		sll1.concat(sll2);
		sll1.toFirst();
		assertTrue(sll1.getObject().equals("a"));
		sll1.next();
		assertTrue(sll1.getObject().equals("b"));
		sll1.next();
		assertTrue(sll1.getObject().equals("c"));
		sll1.next();
		assertTrue(sll1.getObject().equals("d"));
		sll1.next();
		assertTrue(sll1.getObject().equals("e"));
		sll1.next();
		assertTrue(sll1.getObject().equals("f"));
		sll1.next();
		assertTrue(sll1.getObject() == null);
	}
}
