package queue;

public class ExampleQueue {

	public static void main(String[] args) {
		Queue q = new Queue();
		System.out.println(q.front()); // null
		q.enqueue("a");
		System.out.println(q.front()); // a
		q.enqueue("b");
		System.out.println(q.front()); // a
		q.enqueue("c");
		System.out.println(q.front()); // a
		q.dequeue();
		System.out.println(q.front()); // b
		q.dequeue();
		System.out.println(q.front()); // c
		q.dequeue();
		System.out.println(q.isEmpty()); // true
	}

}
