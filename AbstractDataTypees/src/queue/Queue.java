package queue;

public class Queue {
	private Node first;
	private Node last;

	public boolean isEmpty() {
		return first == null;
	}

	public void enqueue(Object o) {
		if (o == null)
			return;

		Node n = new Node(o);
		if (isEmpty()) {
			first = n;
			last = first;
		} else {
			last.setNext(n);
			last = n;
		}

	}

	public void dequeue() {
		if (isEmpty())
			return;

		first = first.getNext();
	}

	public Object front() {
		if (isEmpty())
			return null;
		return first.getContent();
	}
}
