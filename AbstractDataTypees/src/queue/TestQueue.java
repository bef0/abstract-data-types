package queue;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestQueue {

	@Test
	public void testIsEmpty() {
		Queue q = new Queue();
		assertTrue(q.isEmpty());
		q.enqueue("");
		assertTrue(!q.isEmpty());
	}

	@Test
	public void testEnqueue() {
		Queue q = new Queue();
		q.enqueue(null);
		assertTrue(q.isEmpty());
		q.enqueue("a");
		assertTrue(q.front().equals("a"));
		q.enqueue("b");
		assertTrue(q.front().equals("a"));
		q.enqueue("c");
		assertTrue(q.front().equals("a"));
		q.dequeue();
		assertTrue(q.front().equals("b"));
		q.dequeue();
		assertTrue(q.front().equals("c"));
		q.dequeue();
		assertTrue(q.isEmpty());
	}

	@Test
	public void testDequeue() {
		Queue q = new Queue();
		q.dequeue();
		assertTrue(q.isEmpty());
		q.enqueue("a");
		assertTrue(q.front().equals("a"));
		q.enqueue("b");
		assertTrue(q.front().equals("a"));
		q.enqueue("c");
		assertTrue(q.front().equals("a"));
		q.dequeue();
		assertTrue(q.front().equals("b"));
		q.dequeue();
		assertTrue(q.front().equals("c"));
		q.dequeue();
		assertTrue(q.isEmpty());
	}

	@Test
	public void testFront() {
		Queue q = new Queue();
		assertTrue(q.front() == null);
		q.enqueue("a");
		assertTrue(q.front().equals("a"));
		q.enqueue("b");
		assertTrue(q.front().equals("a"));
		q.enqueue("c");
		assertTrue(q.front().equals("a"));
		q.dequeue();
		assertTrue(q.front().equals("b"));
		q.dequeue();
		assertTrue(q.front().equals("c"));
		q.dequeue();
		assertTrue(q.isEmpty());
	}

}
