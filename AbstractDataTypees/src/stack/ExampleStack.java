package stack;

public class ExampleStack {
	public static void main(String[] args)
	{
		Stack s = new Stack();
		System.out.println(s.isEmpty()); // true
		System.out.println(s.top()); // null
		s.push("World!");
		System.out.println(s.top()); // World!
		s.push(" ");
		System.out.println(s.top()); // 
		s.push("Hello");
		while (!s.isEmpty())
		{
			System.out.print(s.top());
			s.pop();
		} // Hello World!
		System.out.println("\n" + s.isEmpty()); // true
		System.out.println(s.top()); // null
	}
}
