package stack;

public class Stack {
	private Node top;

	public boolean isEmpty() {
		return top == null;
	}

	public void push(Object o) {
		if (o == null)
			return;

		Node n = new Node(o);
		n.setNext(top);
		top = n;
	}

	public void pop() {
		if (!isEmpty()) {
			top = top.getNext();
		}
	}

	public Object top() {
		if (isEmpty()) {
			return null;
		}

		return top.getContent();
	}
}
