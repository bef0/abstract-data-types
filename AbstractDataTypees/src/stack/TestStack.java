package stack;

import static org.junit.Assert.*;

import org.junit.*;

public class TestStack {

	

	@Test 
	public void testPush() {
		Stack sll = new Stack();
		sll.push(null);
		assertTrue(sll.top() == null);
		
		sll.push("a");
		assertTrue(sll.top().equals("a"));
		sll.push("b");
		assertTrue(sll.top().equals("b"));
	}

	@Test
	public void testPop() {
		Stack sll = new Stack();
		sll.push("a");
		sll.push("b");
		sll.push("c");
		Object c = sll.top();
		sll.pop();
		Object b = sll.top();
		sll.pop();
		assertTrue(c.equals("c") && b.equals("b"));
	}

	@Test 
	public void testTop() {
		Stack sll = new Stack();
		Object a = sll.top();
		sll.push("");
		Object c = sll.top();
		assertTrue(a == null && c.equals(""));
	}

	@Test 
	public void testIsEmpty() {
		Stack sll = new Stack();
		boolean a = sll.isEmpty();
		sll.push("");
		boolean b = sll.isEmpty();
		assertTrue(a && !b);
	}
}
